<?php

/**
 * Description of DB
 *
 * @author dhouard
 */
class DB {
    
    const ENGINE_MYSQL = 0;
    const ENGINE_POSTGRESQL = 1;
    const ENGINE_SQLITE = 2;
    const ENGINE_ORACLE = 3;
    
    public $debug = false;
    public $error; 
    
    private $dsn;
    private $descriptor;
    private $lastId = false;
    private $rowCount = false;
    
    function __construct($db, $user, $password, $host = 'localhost', $engine = self::ENGINE_MYSQL) {
        
        switch ($engine) {
            case 0: $this->dsn = 'mysql:host='.$host.';dbname='.$db;
                    $this->descriptor = new PDO($this->dsn, $user, $password, 
                                            array(  PDO::ATTR_EMULATE_PREPARES => false, 
                                                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                    break;
            case 1: $this->dsn = 'pgsql:host='.$host.';dbname='.$db;
                    $this->descriptor = new PDO($this->dsn, $user, $password, 
                                            array(  PDO::ATTR_EMULATE_PREPARES => false, 
                                                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                    break;
            case 2: $this->dsn = 'sqlite:'.$db;
                    $this->descriptor = new PDO($this->dsn, array(  PDO::ATTR_EMULATE_PREPARES => false, 
                                                                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                    break;
            case 3: $this->dsn = 'oci:dbname=//'.$host.':1521/'.$db;
                    $this->descriptor = new PDO($this->dsn, $user, $password, 
                                            array(  PDO::ATTR_EMULATE_PREPARES => false, 
                                                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                    break;
                
        }   
        
    }
    
    public function getDsn() {
        
        return $this->dsn;
        
    }
    
    public function getDescriptor() {
        
        return $this->descriptor;
        
    }
    
    public function getLastId() {
        
        return $this->lastId;
        
    }
    
    public function getRowCount() {
        
        return $this->rowCount;
        
    }
    
    /**
     * Launches a query against the DDBB
     * 
     * @param mixed $projection List of fields the query will return. Can be either an array of string or a string
     *                          of fields separated by a comma
     * @param string $table     DDBB table which data will be get from
     * @param string $where     WHERE clause
     * @param string $groupBy   String containing field to group by and, ocasionally, having clause
     * @param string $orderBy   Field whivh will be used to sort the result
     * @param string $limit     Start and lengt of result separated by a comma
     * @return array
     */
    
    public function select($projection, $table, $where = '', $groupBy = '', $orderBy = '', $limit = '') {
        
        $this->error = false;
        $this->rowCount = 0;
        
        if (is_array($projection)) {
            $fields = implode(', ', $projection);
        }
        else {
            $fields = $projection;
        }
        $sql = 'SELECT '.$fields.' FROM '.$table;

        if (!empty($where)) {
            $sql .= ' WHERE '.$where;
        }
        if (!empty($groupBy)) {
            $sql .= ' GROUP BY '.$groupBy;
        }
        if (!empty($orderBy)) {
            $sql .= ' ORDER BY '.$orderBy;
        }
        if (!empty($limit)) {
            $sql .= ' LIMIT '.$limit;
        }
        
        if ($this->debug) 
            Base::trackDebug($sql, $this->debug);

        try {
            $result = $this->descriptor->query($sql);
        }
        catch (Exception $ex) {
            $this->error = $ex;
            $result = false;
        }

        $values = array();
        
        if ($result) {
            while ($row = $result->fetchObject()) {
                $values[] = $row;
            }

        }
        
        return $values;
        
    }
    
    /**
     * Inserts into a table
     * 
     * @param mixed $fields array of columns names
     * @param mixed $values array of values. 
     * @param type $table   Table to insert into
     * @return boolean  
     */
    
    public function insert($table, $fields, $values) {
        
        $this->error = false;
        $this->rowCount = 0;
        
        if (empty($fields) || empty($values) || empty($table)) {
            if (empty($fields)) {
                Try {
                    throw new Exception('Fields array cannot be empty');
                }
                catch (Exception $ex) {
                    $this->error = $ex;
                }
            }
            else {
                if (empty($values)) {
                    try {
                        throw new Exception('Values array cannot be empty');
                    } catch (Exception $ex) {
                        $this->error = $ex;
                    }
                }
                else {
                    if ($empty($table)) {
                        try {
                            throw new Exception('Table not especified');
                        } catch (Exception $ex) {
                            $this->error = $ex;
                        }
                    }
                }
            }
            
            $result = false;
            
        }
        
        if ($result) {
            $sql = 'INSERT INTO '.$table.' ('.implode(', ', $fields).') VALUES ('
                   .substr(str_pad('', (count($fields) * 3), '?, '), 0, -2).')';

            if ($this->debug) 
                Base::trackDebug($sql, $this->debug);

            $statement = $this->descriptor->prepare($sql);

            for ($i = 1; $i <= count($fields); $i++) {
                $statement->bindParam($i, $$fields[$i-1]);
            }

            /* Check if values is an array of arrays, so that we know if one or many registers are given */
            if (!is_array($values[0])) {
                for ($i = 0; $i < count($fields); $i++) {
                    ${$fields[$i]} = $values[$i];
                }

                try {
                $result = $statement->execute();
                $this->rowCount = $result ? $statement->rowCount() : 0;
                }
                catch (Exception $ex) {
                    $this->error = $ex;
                    $result = false;
                }
            }
            else {
                $rowCount = 0;
                $this->descriptor->beginTransaction();
                foreach ($values as $row) {
                    for ($i = 0; $i < count($fields); $i++) {
                        ${$fields[$i]} = $row[$i];
                    }
                    try {
                        $result = $statement->execute();
                        $rowCount += $statement->rowCount();
                    }
                    catch (Exception $ex) {
                        $this->error = $ex;
                        $result = false;
                        break;
                    }
                }
                if ($result) {
                    $this->descriptor->commit();
                    $this->rowCount = $rowCount;
                    $this->lastId = $this->descriptor->lastInsertId();
                }
                else {
                    $this->descriptor->rollBack();
                }
            }
        }
        
        return $result;
        
    }
    
    /**
     * Updates table. Only accepts equal as condition in where clause. For other conditions use self::query instead
     * 
     * @param array $fields     Array of fields to be updated
     * @param array $values     Array of values, one array by data row
     * @param string $table     Table to be updated
     * @param string $filter    Fields wich will be used in where clause
     * @param array $where      Array of values to be used in where clause
     * @return mixed            Number of affected rows or false
     */
    
    public function update($table, $fields, $values, $filter = '', $where = '') {
        
        $this->error = false;
        $this->rowCount = 0;
        
        if (empty($fields) || empty($values) || empty($table)) {
            if (empty($fields)) {
                Try {
                    throw new Exception('Fields array cannot be empty');
                }
                catch (Exception $ex) {
                    $this->error = $ex;
                }
            }
            else {
                if (empty($values)) {
                    try {
                        throw new Exception('Values array cannot be empty');
                    } catch (Exception $ex) {
                        $this->error = $ex;
                    }
                }
                else {
                    if ($empty($table)) {
                        try {
                            throw new Exception('Table not especified');
                        } catch (Exception $ex) {
                            $this->error = $ex;
                        }
                    }
                }
            }
            
            $result = false;
        }
        
        if ($result) {
            $sql = 'UPDATE '.$table.' SET ';
            foreach ($fields as $field) {
                $sql .= $field.' = ?, ';
            }

            $sql = substr($sql, 0, -2);

            if (!empty($filter)) {
                if (empty($where)) {
                    try {
                        throw new Exception('where values cannot be empty');
                    } catch (Exception $ex) {
                        $this->error = $ex;
                        $result = false;
                    }
                }
                else {
                    $sql .= ' WHERE '.$filter.' = ';
                    if (!is_array($where)) {
                        $sql .= $where;
                        $multipleWhere = false;
                    }
                    else {
                        $sql .= '?';
                        $multipleWhere = true;
                    }
                }
            }
            try {
                $statement = $this->descriptor->prepare($sql);
            } catch (Exception $ex) {
                $this->error = $ex;
                $statement = false;
                $result = false;
            }

            if ($statement) {
                for ($i = 1; $i <= count($fields); $i++) {
                    $statement->bindParam($i, ${$fields[$i-1]});
                }

                if ($multipleWhere) 
                    $statement->bindParam ($i, $whereValue);

                $nRow = 0;
                $rowCount = 0;
                foreach ($values as $row) {
                    for ($i = 0; $i < count($fields); $i++) {
                        if (is_array($row)) {
                            ${$fields[$i]} = $row[$i];
                        }
                        else {
                            ${$fields[$i]} = $row;
                        }
                    }
                    if ($multipleWhere) {
                        $whereValue = $where[$nRow];
                        $nRow++;
                    }
                    try {
                        $result = $statement->execute();
                        if ($result) {
                            $rowCount += $statement->rowCount();
                        }
                    }
                    catch (Exception $ex) {
                        $this->error = $ex;
                        $result = false;
                        break;
                    }
                }

                $this->rowCount = $result ? $statement->rowCount() : 0;
            }
        }
        
        return $result;

    }
    
    
    /**
     * Delete from table
     * 
     * @param string $table         Table to delete from
     * @param string $filter        Field for where clause
     * @param mixed $where          String of array containing values for where clause
     * @return boolan
     * @throws Exception
     */
    
    public function delete($table, $filter = '', $where = '') {
        
        $this->error = false;
        $this->rowCount = 0;
        
        if (empty($table)) {
            try {
                throw new Exception('Table cannot be empty');
            } catch (Exception $ex) {
                $this->error = $ex;
                $this->result = false;
            }
        }
        else {
            $sql = 'DELETE FROM '.$table;
            if (!empty($filter)) {
                if (empty($where)) {
                    try {
                        throw new Exception('where values cannot be empty');
                    } 
                    catch (Exception $ex) {
                        $this->error = $ex;
                        $this->result = false;
                    }
                }
                else {
                    $sql .= ' WHERE '.$filter.' = ?';
                    $whereClause = true;
                }
            }
            else {
                $whereClause = false;
            }
            
            try {
                $statement = $this->descriptor->prepare($sql);
            }
            catch (Exception $ex) {
                $this->error = $ex;
                $this->result = false;
            }
            
            if ($statement) {
                if (!$whereClause) {
                    try {
                        $result = $statement->execute();
                        $this->rowCount = $result ? $statement->rowCount() : 0;
                    }
                    catch (Exception $ex) {
                        $this->error = $ex;
                        $this->result = false;
                    }
                }
                else {
                    if (!is_array($where)) {
                        $statement->bindParam(1, $where);
                        try {
                            $result = $statement->execute();
                            $this->rowCount = $result ? $statement->rowCount() : 0;
                        }
                        catch (Exception $ex) {
                            $this->error = $ex;
                            $this->result = false;
                        }
                    }
                    else {
                        $statement->bindParam(1, $value); 
                        $rowCount = 0;
                        $this->descriptor->beginTransaction();
                        foreach ($where as $value) {
                            try {
                                $result = $statement->execute();
                                if ($result) {
                                    $rowCount += $statement->rowCount();
                                }
                            } 
                            catch (Exception $ex) {
                                $this->error = $ex;
                                $this->result = false;
                                break;
                            }
                        }
                        if ($result) {
                            $this->descriptor->commit();
                            $this->rowCount = $result ? $statement->rowCount() : 0;
                        }
                        else {
                            $this->descriptor->rollBack();
                        }
                    }
                }
                
            }
            
        }
        
        return $result;
        
    }
    
    public function query($sql) {
        
        if (empty($sql)) {
            try {
                throw new Exception('SQL query cannot be empty');
            } 
            catch (Exception $ex) {
                $this->error = $ex;
                $result = false;
            }
        }
        else {
            try {
                $statement = $this->descriptor->prepare($sql);
                $result = $statement->execute();
                $this->rowCount = $result ? $statement->rowCount() : 0;
            } catch (Exception $ex) {
                $this->error = $ex;
                $result = false;
            }
            
        }
        
        if (preg_match('/SELECT/', strtoupper($sql)) && $result) {
            $values = array();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $row) {
                $values[] = $row;
            }
            
            $result = $values;
        }
        
        return $result;
        
    }
        
}
