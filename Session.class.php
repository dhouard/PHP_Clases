<?php

/**
 * Session management
 *
 * @author Jose María Ramírez
 */
class Session {
    
    static $session = null;
    private $cookies = false;
    
    static public function getSession() {
        
        if (self::$session == null) {
            self::$session = new Session;
        }
        
        return self::$session;
    }
   
    private function __construct() {
        
        session_start();
        if (isset($_COOKIE['PHPSESSID'])) {
            $this->cookies = true;
        }
        $_SESSION["sessionStart"] = microtime(true);
        
    }
    
    public function isActive() {
        
        return session_status() == PHP_SESSION_ACTIVE ? true : false;
        
    }
    
    public function acceptCookies() {
        
        return $this->cookies;
        
    }
    
	
    public function exist($index) {
        
        return isset($_SESSION[$index]);
        
    }
	
    public function get($index) {
        
        return isset($_SESSION[$index]) ? $_SESSION[$index] : false;
        
    }
    
    public function set($index, $value = '') {
        
        $_SESSION[$index] = $value;
        return true;
        
    }
    
    public function remove($index) {
        
        if (isset($_SESSION[$index])) {
            unset($_SESSION[$index]);
            return true;
        }
        else {
            return false;
        }
    }
    
    public function emptyVar($product) {
        
        if (isset($_SESSION[$product])) {
            if (is_array($_SESSION[$product])) {
                $_SESSION[$product] = array();
            }
            else { 
                $_SESSION[$product] = '';
            }
            return true;
        }
        else {
            return false;
        }
        
    }
    
    public function close() {
        
        session_destroy();
        session_unset();
        
    }
    
}


